package com.example.projekt.klase;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class IskoristenaUlaznica extends Ulaznica implements Serializable {
    public LocalDate datumKoristenja;
    public LocalTime vrijemeKoristenja;

    public IskoristenaUlaznica(Ulaznica ulaznica, LocalDate datum, LocalTime vrijeme){
        super(ulaznica.id, ulaznica.datumKupnje, ulaznica.vrijemeKupnje);
        this.datumKoristenja=datum;
        this.vrijemeKoristenja=vrijeme;
    }
}
