package com.example.projekt.klase;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import static java.sql.DriverManager.println;


public class Ulaznica  implements Serializable{
    public int id;
    public LocalDate datumKupnje;
    public LocalTime vrijemeKupnje;

    public Ulaznica(int id, LocalDate datum, LocalTime vrijeme){
        this.id=id;
        this.datumKupnje=datum;
        this.vrijemeKupnje=vrijeme;
    }

}