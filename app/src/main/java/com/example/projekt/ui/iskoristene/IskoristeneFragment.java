package com.example.projekt.ui.iskoristene;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;
import com.example.projekt.klase.IskoristenaUlaznica;
import com.example.projekt.klase.Ulaznica;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class IskoristeneFragment extends Fragment {

    private IskoristeneViewModel iskoristeneViewModel;
    ArrayList<IskoristenaUlaznica> iull;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        iskoristeneViewModel =
                new ViewModelProvider(this).get(IskoristeneViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dostupne, container, false);
        ucitajIUlaznice();
        if(iull !=null) prikaziUlaznice(root);
        return root;
    }



    public void ucitajIUlaznice() {
        try {
            FileInputStream fis = getActivity().openFileInput("IUlaznice.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            iull = (ArrayList<IskoristenaUlaznica>) is.readObject();
            is.close();
            fis.close();
        }catch (FileNotFoundException | ClassNotFoundException e){
            iull =null;
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void prikaziUlaznice(View root){

        ListView liUlaznice;
        liUlaznice = root.findViewById(R.id.lista_ulaznice);
        liUlaznice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IskoristenaUlaznica u= iull.get(position);
                Bundle b = new Bundle();
                b.putString("id",String.valueOf(u.id));
                b.putString("dat",u.datumKupnje.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                b.putString("vri",u.vrijemeKupnje.format(DateTimeFormatter.ofPattern("HH:mm")));
                b.putString("datk",u.datumKoristenja.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                b.putString("vrik",u.vrijemeKoristenja.format(DateTimeFormatter.ofPattern("HH:mm")));
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_iuinfo, b);
            }
        });
        IUlazniceAdapter ulAdapter = new IUlazniceAdapter(getActivity(), iull);
        liUlaznice.setAdapter(ulAdapter);
    }

    public class IUlazniceAdapter extends ArrayAdapter<IskoristenaUlaznica> {
        public IUlazniceAdapter(Context context, ArrayList<IskoristenaUlaznica> iulaznice){
            super(context, R.layout.item_iulaznica, iulaznice);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            IskoristenaUlaznica ul = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_iulaznica,parent,false);
            }

            TextView id = convertView.findViewById(R.id.id_txt);
            id.setText(getContext().getResources().getString(R.string.txtId)+ul.id);

            return convertView;
        }
    }
}
