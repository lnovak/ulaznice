package com.example.projekt.ui.uinfo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;

public class UInfoFragment extends Fragment {

    private UInfoViewModel uInfoViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        uInfoViewModel =
                new ViewModelProvider(this).get(UInfoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_uinfo, container, false);

        Bundle bundle= this.getArguments();
        if(bundle!= null){
            TextView id = root.findViewById(R.id.id);
            id.setText(getContext().getResources().getString(R.string.txtId)+bundle.getString("id"));
            TextView dat = root.findViewById(R.id.dat);
            dat.setText(getContext().getResources().getString(R.string.txtDatumKupnje)+"\n"+bundle.getString("dat"));
            TextView vri = root.findViewById(R.id.vri);
            vri.setText(getContext().getResources().getString(R.string.txtVrijemeKupnje)+"\n"+bundle.getString("vri"));

        }

        return root;
    }
}