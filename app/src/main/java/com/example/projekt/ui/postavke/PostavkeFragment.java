package com.example.projekt.ui.postavke;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class PostavkeFragment extends Fragment {

    private PostavkeViewModel postavkeViewModel;
    private String tema;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        postavkeViewModel =
                new ViewModelProvider(this).get(PostavkeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_postavke, container, false);
        final TextView textView = root.findViewById(R.id.text_slideshow);
        postavkeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        SwitchCompat swSvijetlo=root.findViewById(R.id.switchSvijetlo);
        SwitchCompat swTamno=root.findViewById(R.id.switchTamno);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                getContext().openFileInput("Tema.txt"), StandardCharsets.UTF_8))) {
            tema = reader.readLine();
            if(tema.contains("svijetlo")){
                swSvijetlo.setChecked(true);
                swTamno.setChecked(false);
            }else{
                swSvijetlo.setChecked(false);
                swTamno.setChecked(true);
            }
        }catch (FileNotFoundException e) {
            try {
                OutputStreamWriter out = new OutputStreamWriter(getContext().openFileOutput("Tema.txt", MODE_PRIVATE));
                out.write("svijetlo");
                out.close();
                tema="svijetlo";
                swSvijetlo.setChecked(true);
                swTamno.setChecked(false);
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        swSvijetlo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(swSvijetlo.isChecked()==true){
                    swTamno.setChecked(false);
                    tema="svijetlo";
                }else{
                    swTamno.setChecked(true);
                    tema="tamno";
                }
            }
        });
        swTamno.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(swTamno.isChecked()==true){
                    swSvijetlo.setChecked(false);
                    tema="tamno";
                }else{
                    swSvijetlo.setChecked(true);
                    tema="svijetlo";
                }
            }
        });

        Button btnSpremi=root.findViewById(R.id.btnSpremi);
        btnSpremi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try {
                    OutputStreamWriter out = new OutputStreamWriter(getContext().openFileOutput("Tema.txt", MODE_PRIVATE));
                    out.write(tema);
                    out.close();
                }catch (Exception ex) {
                    ex.printStackTrace();
                }
                if(tema.contains("svijetlo")){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_home);
            }
        });
        return root;
    }
}