package com.example.projekt.ui.skeniraj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class SkenirajFragment extends Fragment {

    private SkenirajViewModel skenirajViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        skenirajViewModel =
                new ViewModelProvider(this).get(SkenirajViewModel.class);
        View root = inflater.inflate(R.layout.fragment_skeniraj, container, false);
        final TextView textView = root.findViewById(R.id.text_slideshow);
        skenirajViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        Button btnIskoristi=root.findViewById(R.id.btnIskoristi);
        btnIskoristi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                IntentIntegrator iskoristi = new IntentIntegrator(getActivity());
                iskoristi.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                iskoristi.setPrompt(getResources().getString(R.string.txtSkeniraj));
                iskoristi.setBeepEnabled(true);
                iskoristi.setOrientationLocked(false);
                iskoristi.setRequestCode(1);
                iskoristi.initiateScan();

            }
        });
        Button btnKupi=root.findViewById(R.id.btnKupi);
        btnKupi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                IntentIntegrator iskoristi = new IntentIntegrator(getActivity());
                iskoristi.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                iskoristi.setPrompt(getResources().getString(R.string.txtSkeniraj));
                iskoristi.setBeepEnabled(true);
                iskoristi.setOrientationLocked(false);
                iskoristi.setRequestCode(2);
                iskoristi.initiateScan();

            }
        });
        return root;
    }
}