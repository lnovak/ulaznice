package com.example.projekt.ui.iskoristene;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class IskoristeneViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public IskoristeneViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}