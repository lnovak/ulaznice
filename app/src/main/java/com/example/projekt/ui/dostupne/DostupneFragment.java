package com.example.projekt.ui.dostupne;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;
import com.example.projekt.klase.IskoristenaUlaznica;
import com.example.projekt.klase.Ulaznica;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DostupneFragment extends Fragment {

    private DostupneViewModel dostupneViewModel;
    ArrayList<Ulaznica> ull;
    ArrayList<IskoristenaUlaznica> iull;
    ArrayList<Ulaznica> ulZaPrikaz=new ArrayList<Ulaznica>();


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dostupneViewModel =
                new ViewModelProvider(this).get(DostupneViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dostupne, container, false);
        ucitajUlaznice();
        ucitajIUlaznice();
        if(ull !=null) prikaziUlaznice(root);
        return root;
    }

    public void ucitajUlaznice(){
        try {
            FileInputStream fis = getActivity().openFileInput("Ulaznice.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            ull = (ArrayList<Ulaznica>) is.readObject();
            is.close();
            fis.close();
        }catch (FileNotFoundException | ClassNotFoundException e){
            ull =null;
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void ucitajIUlaznice() {
        try {
            FileInputStream fis = getActivity().openFileInput("IUlaznice.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            iull = (ArrayList<IskoristenaUlaznica>) is.readObject();
            is.close();
            fis.close();
        }catch (FileNotFoundException | ClassNotFoundException e){
            iull =null;
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void prikaziUlaznice(View root){
        ulZaPrikaz.clear();
        for(Ulaznica ul:ull){
            ulZaPrikaz.add(ul);
        }


        if(iull!=null) {
            for (IskoristenaUlaznica iu : iull) {
                for (Ulaznica u : ull) {
                    if (u.id == iu.id) {
                        ulZaPrikaz.remove(u);
                    }
                }
            }
        }


        ListView liUlaznice;
        liUlaznice = root.findViewById(R.id.lista_ulaznice);
        liUlaznice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ulaznica u= ull.get(position);
                Bundle b = new Bundle();
                b.putString("id",String.valueOf(u.id));
                b.putString("dat",u.datumKupnje.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                b.putString("vri",u.vrijemeKupnje.format(DateTimeFormatter.ofPattern("HH:mm")));
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_uinfo, b);
            }
        });
        UlazniceAdapter ulAdapter = new UlazniceAdapter(getActivity(), ulZaPrikaz);
        liUlaznice.setAdapter(ulAdapter);
    }

    public class UlazniceAdapter extends ArrayAdapter<Ulaznica> {
        public UlazniceAdapter(Context context, ArrayList<Ulaznica> ulaznice){
            super(context, R.layout.item_ulaznica, ulaznice);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Ulaznica ul = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_ulaznica,parent,false);
            }

            TextView id = convertView.findViewById(R.id.id_txt);
            id.setText(getContext().getResources().getString(R.string.txtId)+ul.id);

            return convertView;
        }
    }
}
