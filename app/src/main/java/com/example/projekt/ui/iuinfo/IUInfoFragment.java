package com.example.projekt.ui.iuinfo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.projekt.R;
import com.example.projekt.ui.uinfo.UInfoViewModel;

public class IUInfoFragment extends Fragment {

    private IUInfoViewModel iuInfoViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        iuInfoViewModel =
                new ViewModelProvider(this).get(IUInfoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_iuinfo, container, false);

        Bundle bundle= this.getArguments();
        if(bundle!= null){
            TextView id = root.findViewById(R.id.id);
            id.setText(getContext().getResources().getString(R.string.txtId)+bundle.getString("id"));
            TextView dat = root.findViewById(R.id.dat);
            dat.setText(getContext().getResources().getString(R.string.txtDatumKupnje)+"\n"+bundle.getString("dat"));
            TextView vri = root.findViewById(R.id.vri);
            vri.setText(getContext().getResources().getString(R.string.txtVrijemeKupnje)+"\n"+bundle.getString("vri"));
            TextView datk = root.findViewById(R.id.idat);
            datk.setText(getContext().getResources().getString(R.string.txtDatumKoristenja)+"\n"+bundle.getString("datk"));
            TextView vrik = root.findViewById(R.id.ivri);
            vrik.setText(getContext().getResources().getString(R.string.txtVrijemeKoristenja)+"\n"+bundle.getString("vrik"));

        }

        return root;
    }
}