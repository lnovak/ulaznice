package com.example.projekt.ui.ulaznice;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekt.R;

public class UlazniceFragment extends Fragment {

    private UlazniceViewModel ulazniceViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ulazniceViewModel =
                new ViewModelProvider(this).get(UlazniceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ulaznice, container, false);
        final TextView textView = root.findViewById(R.id.text_slideshow);
        ulazniceViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        Button btnDostupne=root.findViewById(R.id.btnDostupne);
        btnDostupne.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_dostupne);
            }
        });

        Button btnIskoristene=root.findViewById(R.id.btnIskoristene);
        btnIskoristene.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_iskoristene);
            }
        });


        return root;
    }
}