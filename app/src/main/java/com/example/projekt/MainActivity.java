package com.example.projekt;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentContainer;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import com.example.projekt.klase.IskoristenaUlaznica;
import com.example.projekt.klase.Ulaznica;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Locale;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ArrayList<Ulaznica> ulaznice=new ArrayList<Ulaznica>();
    private ArrayList<IskoristenaUlaznica> iUlaznice=new ArrayList<IskoristenaUlaznica>();


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                openFileInput("Tema.txt"), StandardCharsets.UTF_8))) {
            String tema = reader.readLine();
            if(tema.contains("svijetlo")){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }else{
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }catch (FileNotFoundException e) {
            try {
                OutputStreamWriter out = new OutputStreamWriter(openFileOutput("Tema.txt", MODE_PRIVATE));
                out.write("svijetlo");
                out.close();
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        ucitajUlaznice();
        ucitajIUlaznice();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ucitajUlaznice();
                if(ulaznice!=null) {
                    Toast.makeText(MainActivity.this, String.valueOf(ulaznice.size()), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(MainActivity.this, "null je", Toast.LENGTH_LONG).show();
                }
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,  R.id.nav_skeniraj, R.id.nav_ulaznice)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            navController.navigate(R.id.nav_postavke);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode != 1 && requestCode !=2) && requestCode != IntentIntegrator.REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);

        if(result.getContents() == null) {
            Intent originalIntent = result.getOriginalIntent();
            if (originalIntent == null) {
                Toast.makeText(this, getResources().getString(R.string.txtOtkazano), Toast.LENGTH_LONG).show();
            } else if(originalIntent.hasExtra(Intents.Scan.MISSING_CAMERA_PERMISSION)) {
                Toast.makeText(this, getResources().getString(R.string.txtOtkazanoKamera), Toast.LENGTH_LONG).show();
            }
        } else {
            if(requestCode==1){
                iskoristiUlaznice(Integer.parseInt(result.getContents()));
            }else{
                kupiUlaznice(Integer.parseInt(result.getContents()));
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void kupiUlaznice(int br){
        Toast.makeText(this, br+" "+getResources().getString(R.string.txtKupljeno), Toast.LENGTH_SHORT).show();
        ArrayList<Ulaznica> aUl=ulaznice;
        if(aUl==null){
            aUl=new ArrayList<Ulaznica>();
        }
        for(int i=0;i<br;i++){
            Ulaznica ul=new Ulaznica(dodijeliId("UId"), LocalDate.now(), LocalTime.now());
            aUl.add(ul);
        }
        try {
            FileOutputStream fos = this.openFileOutput("Ulaznice.txt",  MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(aUl);
            os.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void iskoristiUlaznice(int br) {
        int razlika;
        ucitajUlaznice();
        ucitajIUlaznice();
        if(ulaznice==null ){
            Toast.makeText(this, getResources().getString(R.string.txtNedovoljno) , Toast.LENGTH_LONG).show();
            ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen.startTone(ToneGenerator.TONE_CDMA_PIP,150);
            return;
        }else if(iUlaznice==null){
            razlika=ulaznice.size()-br;
        }else{
            razlika=ulaznice.size()-iUlaznice.size()-br;
        }

        if(razlika>=0) {
            Toast.makeText(this, br+" "+getResources().getString(R.string.txtIskoristeno), Toast.LENGTH_SHORT).show();
            ArrayList<IskoristenaUlaznica> aIUl = iUlaznice;
            if(aIUl==null){
                aIUl=new ArrayList<IskoristenaUlaznica>();
            }
            for (int i = 0; i < br; i++) {
                IskoristenaUlaznica iul = new IskoristenaUlaznica(ulaznice.get(dodijeliId("IUId")-1), LocalDate.now(), LocalTime.now());
                aIUl.add(iul);
            }
            try {
                FileOutputStream fos = this.openFileOutput("IUlaznice.txt", MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(aIUl);
                os.close();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, getResources().getString(R.string.txtNedovoljno) , Toast.LENGTH_LONG).show();
            ToneGenerator toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen.startTone(ToneGenerator.TONE_CDMA_PIP,150);
        }
    }

    public int dodijeliId(String dat){
        int trenutniId=1;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                openFileInput(dat+".txt"), StandardCharsets.UTF_8))) {

            String red = reader.readLine();
            if(red!="" && red!=null){
                trenutniId=Integer.parseInt(red)+1;
                OutputStreamWriter out = new OutputStreamWriter(openFileOutput(dat+".txt", MODE_PRIVATE));
                out.write(String.valueOf(trenutniId));
                out.close();
            }
        }catch (FileNotFoundException e) {
            try {
                OutputStreamWriter out = new OutputStreamWriter(openFileOutput(dat+".txt", MODE_PRIVATE));
                out.write(String.valueOf(trenutniId));
                out.close();

            }catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return trenutniId;
    }

    public void ucitajUlaznice() {
        try {
            FileInputStream fis = this.openFileInput("Ulaznice.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            ulaznice = (ArrayList<Ulaznica>) is.readObject();
            is.close();
            fis.close();
        }catch (FileNotFoundException | ClassNotFoundException e){
            ulaznice=null;
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void ucitajIUlaznice() {
        try {
            FileInputStream fis = this.openFileInput("IUlaznice.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            iUlaznice = (ArrayList<IskoristenaUlaznica>) is.readObject();
            is.close();
            fis.close();
        }catch (FileNotFoundException | ClassNotFoundException e){
            iUlaznice=null;
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}